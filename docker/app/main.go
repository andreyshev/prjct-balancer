package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	// create file server handler

	router := http.NewServeMux()
	router.HandleFunc("/", Balance)

	// start HTTP server with `fs` as the default handler
	log.Fatal(http.ListenAndServe(":9000", router))
}

func Balance(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<b>SERVER_REGION: " + os.Getenv("SERVER_REGION") + "</br>\n"))
	w.Write([]byte("X-Country: " + r.Header.Get("X-Country") + "\n"))
	w.Write([]byte("X-Forwarded-For: " + r.Header.Get("X-Forwarded-For") + "\n"))
	w.Write([]byte("X-Real-IP: " + r.Header.Get("X-Real-IP") + "\n"))
	w.Write([]byte("X-Client-IP: " + r.Header.Get("X-Client-IP") + "\n"))
}
